import fs from 'fs';

const readFileContent = (filePath) => {
  try {
    return fs.readFileSync(filePath).toString();
  } catch (e) {
    return '';
  }
};

export default readFileContent;
