import path from 'path';
import fs from 'fs';

const resolveApp = (relativePath) =>
  path.resolve(fs.realpathSync(process.cwd()), relativePath);

export default resolveApp;
