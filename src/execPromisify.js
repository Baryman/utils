import { exec as execCP } from 'child_process';

const execPromisify = (command) =>
  new Promise((resolve, reject) => {
    execCP(command, (error, stdout, stderr) => {
      if (error) {
        reject(error);
      }

      if (stderr) {
        reject(stderr);
      }

      resolve(stdout);
    });
  });

export default execPromisify;
